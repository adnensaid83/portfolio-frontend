export { default as X } from "./X";
export { default as ArrowRight } from "./ArrrowRight";
export { default as Behance } from "./Behance";
export { default as Instagram } from "./Instagrem";
export { default as Linkedin } from "./Linkedin";
export { default as List } from "./List";
export { default as Moon } from "./Moon";
export { default as Sun } from "./Sun";
