import { FieldProps, useField } from "@formiz/core";
import { FormGroupProps } from "./FormGroup";
import Spin from "../assets/Spin";

type Value = string;

export type FieldInputProps<FormattedValue = Value> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps & {
    isLoading?: boolean;
  };

export const FieldTextarea = <FormattedValue = Value,>(
  props: FieldInputProps<FormattedValue>
) => {
  const {
    id,
    isValid,
    isTouched,
    isRequired,
    isValidating,
    isProcessing,
    isDebouncing,
    isReady,
    isPristine,
    setValue,
    value,
    setIsTouched,
    otherProps: {
      children,
      label,
      placeholder,
      autoFocus = false,
      isLoading = false,
    },
  } = useField(props);

  return (
    <div className="col-span-1 md:col-span-2">
      <label
        htmlFor={id}
        className="block mb-4 font-medium text-base text-dark"
      >
        {label} {isRequired && "*"}
      </label>
      <textarea
        className={`w-full p-4 mb-4 rounded-xl border-2 text-base border-dark ${
          !isValid && isTouched && "border-accent"
        } ${!isProcessing && isValid && "border-green200"}`}
        id={id}
        rows={5}
        value={value ?? ""}
        onChange={(e) => setValue(e.target.value)}
        onFocus={() => setIsTouched(false)}
        onBlur={() => setIsTouched(true)}
        placeholder={placeholder}
        autoFocus={autoFocus}
      />
      <div>
        {!isReady && "🚧"}
        {isReady && isProcessing && "⏳"}
        {/* {!isValid && isTouched && "❌"} */}
        {/* {!isProcessing && isValid && "✅"} */}
      </div>
      <div>
        {isDebouncing && "✋🏼"}
        {isValidating || isLoading ? <Spin /> : isPristine && "✨"}
      </div>
      {children}
    </div>
  );
};
