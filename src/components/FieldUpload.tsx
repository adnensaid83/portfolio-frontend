import { ChangeEvent } from "react";
import { FieldProps, useField } from "@formiz/core";
import { FiPaperclip } from "react-icons/fi";
import { FormGroup, FormGroupProps } from "./FormGroup";

type Value = {
  name: string;
  size: number;
  type: string;
  lastModified: number;
  lastModifiedDate: Date;
  file: File;
};

export type FieldUploadProps<FormattedValue> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps;

export const FieldUpload = <FormattedValue = Value,>(
  props: FieldUploadProps<FormattedValue>
) => {
  const {
    errorMessage,
    id,
    isRequired,
    setValue,
    value,
    shouldDisplayError,
    otherProps: { children, label, helper, ...rest },
  } = useField(props, {
    validations: [
      {
        handler: (value) => (value?.size ?? 0) < 1024 * 1024,
        message: "File limit exceeded (1MB)",
      },
    ],
  });

  const formGroupProps = {
    errorMessage,
    helper,
    id,
    isRequired,
    label,
    showError: shouldDisplayError,
    ...rest,
  };

  const handleChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    const file = target.files?.[0];

    if (!file) {
      setValue(null);
      return;
    }

    setValue({
      name: file.name,
      size: file.size,
      type: file.type,
      lastModified: file.lastModified,
      lastModifiedDate: new Date(file.lastModified),
      file,
    });
  };

  return (
    <FormGroup {...formGroupProps}>
      <label
        className={`relative bg-white flex items-center px-3 py-4 rounded-xl border transition duration-200 text-gray-600 text-base cursor-pointer ${
          shouldDisplayError
            ? "border-2 border-red-500 shadow-outline-red"
            : "border-2 border-dark"
        } hover:bg-blue-50 hover:border-blue-600 focus-within:bg-blue-50 focus-within:border-blue-600`}
      >
        <input
          className="opacity-0 absolute top-0 left-0 w-0"
          type="file"
          id={id}
          onChange={handleChange}
        />
        <FiPaperclip className="mr-2" />
        {value?.name || "sélectionnez une image"}
      </label>
      {children}
    </FormGroup>
  );
};
