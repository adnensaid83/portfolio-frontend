import { ReactNode } from "react";
import ExclamationCircle from "../assets/ExclamationCircle";

export type FormGroupProps = {
  children?: ReactNode;
  errorMessage?: ReactNode;
  helper?: ReactNode;
  id?: string;
  isRequired?: boolean;
  length?: number;
  label?: ReactNode;
  type?: string;
  size?: string;
  autoFocus?: boolean;
  placeholder?: string;
  order?: number;
  showError?: boolean;
};

export const FormGroup = ({
  children,
  errorMessage,
  helper,
  id,
  isRequired,
  label,
  showError,
}: FormGroupProps) => {
  return (
    <div className={`relative `}>
      <label
        htmlFor={id}
        className="block mb-4 font-medium text-base text-dark"
      >
        {label} {isRequired && "*"}
      </label>
      <div className="">{children}</div>
      {!!helper && <span>{helper}</span>}
      {!!errorMessage && showError && (
        <div className="text-accent flex items-center gap-2">
          <div className="w-6">
            <ExclamationCircle />
          </div>
          {errorMessage}
        </div>
      )}
    </div>
  );
};
