import ExclamationCircle from "../assets/ExclamationCircle";
import { FormGroupProps } from "./FormGroup";

export const FormGroupCaptcha = ({
  children,
  errorMessage,
  helper,
  label,
  order,
  showError,
}: FormGroupProps) => (
  <div className="relative">
    <div className="flex items-center gap-4 mb-12">
      <span className="flex items-center justify-center text-xl border-2 border-orange w-16 h-16 shrink-0 rounded-full text-white bg-orange">
        {order}
      </span>
      <p className="mb-0 uppercase font-medium text-orange">{label}</p>
    </div>
    <div className={`grid gap-8 mb-12 grid-cols-1`}>{children}</div>
    {!!helper && <span>{helper}</span>}
    {!!errorMessage && showError && (
      <div className="absolute right-0 text-accent flex items-center gap-2">
        <div className="w-6">
          <ExclamationCircle />
        </div>
        {errorMessage}
      </div>
    )}
  </div>
);
