import React, { ReactNode, RefObject, useRef } from "react";
import { List, X } from "../assets";
import { RegularList } from "./RegularList";
import SocialListItem from "./SocialListItem";
import onPress from "../utils/scrollFunction";
import { Link } from "react-router-dom";
import { SocialI } from "../interfaces/Social.interface";
import { Image } from "react-datocms";
import { CoverImageI } from "../interfaces/Project.interface";

export type PageProps = {
  id?: string;
  name: string;
  slug: string;
  order?: number;
  onCloseMenu?: () => void;
  nodeRef?: RefObject<any>;
};
type NavbarProps = {
  theme: string;
  handleTheme: (v: string) => void;
  logo: CoverImageI;
  pages: PageProps[];
  address: string;
  socials: SocialI[];
  phone: string;
};
export default function Navbar({
  theme,
  logo,
  pages,
  address,
  socials,
  phone,
}: NavbarProps) {
  const collapseNavbarMenuRef = useRef<HTMLDivElement | null>(null);

  const handleCollapseNavbarMenu = () => {
    const collapse = collapseNavbarMenuRef.current;
    collapse?.classList.toggle("hidden");
    // animation nav-item
    allPages.forEach((p: PageProps) => {
      p.nodeRef && p.nodeRef.current?.classList.toggle("item-enter");
    });
  };
  const allPages = pages.map((p: PageProps) => ({
    name: p.name,
    slug: p.slug,
    nodeRef: React.createRef<HTMLLIElement | null>(),
    onCollapseNavbar: handleCollapseNavbarMenu,
    onPress: onPress,
  }));

  return (
    <header className="sticky top-0 z-10 shadow-nav backdrop-blur bg-header">
      <div className="max-w-[1200px] mx-auto">
        <nav className="flex items-center justify-between py-4 px-12 lg:justify-start lg:py-6 md:gap-20">
          <div className="max-w-[42px] rounded-xl overflow-hidden -rotate-10 ">
            <Image data={logo.responsiveImage} />
          </div>
          <div className="flex lg:hidden">
            <NavbarToggler
              imgCompnent={<List />}
              onCollapseNavbar={handleCollapseNavbarMenu}
              className={` w-[36px] h-[36px] p-0 focus:outline-0 ${
                theme === "dark" ? "text-white" : "text-dark"
              }`}
            />
          </div>
          <div className="hidden lg:flex-1 lg:flex">
            <div className="relative flex-1 flex items-center justify-between gap-6 font-light text-[15px]">
              <ul className="flex">
                <RegularList
                  items={allPages}
                  resourceName="page"
                  itemComponent={NavbarListItem}
                />
              </ul>
              <ul className="flex gap-4">
                <RegularList
                  items={socials}
                  resourceName="social"
                  itemComponent={SocialListItem}
                />
                {/* <SwitchBtn theme={theme} onTheme={handleTheme} /> */}
              </ul>
            </div>
          </div>
        </nav>
        <div className="hidden lg:hidden" ref={collapseNavbarMenuRef}>
          <div
            className={`fixed min-h-screen inset-y-0 right-0 left-0 top-0 bottom-0 z-40 w-full flex flex-col overflow-y-auto ${
              theme === "dark" ? "bg-dark" : "bg-white"
            } `}
          >
            <div className="flex justify-between items-center py-4 px-12 lg:py-6">
              <div className="max-w-[42px] rounded-xl overflow-hidden -rotate-10 ">
                <Image data={logo.responsiveImage} />
              </div>
              <div className="flex lg:hidden">
                <NavbarToggler
                  imgCompnent={<X />}
                  onCollapseNavbar={handleCollapseNavbarMenu}
                  className={` w-[36px] h-[36px] p-0 focus:outline-0 ${
                    theme === "dark" ? "text-white" : "text-dark"
                  }`}
                />
              </div>
            </div>
            <div className="flex-1 flex flex-col justify-center gap-12">
              <ul className="list-none text-center font-light text-xl space-y-0">
                <RegularList
                  items={allPages}
                  resourceName="page"
                  itemComponent={NavbarListItemSm}
                />
              </ul>
              <ul className="flex gap-x-5 list-none justify-center">
                <RegularList
                  items={socials}
                  resourceName="social"
                  itemComponent={SocialListItem}
                />
              </ul>
              {/*               <ul className="flex justify-center">
                <li>
                  <SwitchBtn theme={theme} onTheme={handleTheme} />
                </li>
              </ul> */}
            </div>
            <div className="flex-none flex justify-between px-12 pt-8 pb-20">
              <div className="w-[6rem]">
                <h6 className="text-xl font-extralight"> ADRESSE </h6>
                <address className="not-italic">
                  <p>{address}</p>
                </address>
              </div>
              <div className="">
                <h6 className="text-xl font-extralight"> CONTACT </h6>
                <p className="-tracking-2">{phone}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}
type NavbarToggler = {
  imgCompnent: ReactNode;
  onCollapseNavbar: () => void;
  className: string;
};
function NavbarToggler({
  imgCompnent,
  onCollapseNavbar,
  className,
}: NavbarToggler) {
  return (
    <button className={className} type="button" onClick={onCollapseNavbar}>
      {imgCompnent}
    </button>
  );
}

interface NavbarListItemProps {
  page: any;
  active: string;
}
export function NavbarListItem({ page }: NavbarListItemProps) {
  const { name, slug, nodeRef, onCollapseNavbar, onPress } = page;
  return (
    <li
      className={`mt-0 font-semibold text-2xl hover:opacity-80 py-6 md:text-xl lg:px-8 lg:py-2 lg:text-l`}
      ref={nodeRef}
      data-to-scrollspy-id={slug}
    >
      <Link
        to={`#${slug}`}
        onClick={(e) => {
          onCollapseNavbar();
          onPress(e);
        }}
      >
        {name}
      </Link>
    </li>
  );
}

interface NavbarListItemSmProps {
  page: any;
}
function NavbarListItemSm({ page }: NavbarListItemSmProps) {
  const { name, slug, nodeRef, onCollapseNavbar, onPress } = page;
  return (
    <li
      className={`mt-0 font-medium text-l hover:opacity-80 py-4`}
      ref={nodeRef}
    >
      <Link
        to={`/#${slug}`}
        onClick={(e) => {
          onCollapseNavbar();
          onPress(e);
        }}
      >
        {name}
      </Link>
    </li>
  );
}

/* type SwitchBtnProps = {
  theme: string;
  onTheme: (t: string) => void;
};

function SwitchBtn({ theme, onTheme }: SwitchBtnProps) {
  return (
    <label className={`relative p-0 rounded-full cursor-pointer`}>
      <input
        type="checkbox"
        checked={theme === "light"}
        onChange={(e) => {
          onTheme(e.target.checked ? "light" : "dark");
        }}
        className="absolute opacity-0 w-0 h-0"
      />
      <div
        className={`w-[36px] h-[36px] flex items-center justify-center border-dark border-[2px] rounded-full p-3 ${
          theme === "light" ? "bg-dark text-white" : "bg-white text-orangeDark"
        }`}
      >
        {theme === "light" ? <Moon /> : <Sun />}
      </div>
    </label>
  );
}
 */
