import { Variants, useScroll, motion } from "framer-motion";
import { useContext, useRef, useState } from "react";
import { ThemeContext } from "../routes/root";
import { Underline } from "./TitleSection";

const parcoursData = [
  {
    compagny: "Thunder express",
    logo: "",
    periode: "Janvier 2023 - Aôut2023",
    poste: "Développeur Front-end",
    description:
      "Mon rôle principal consistait à assurer l'intégration des maquettes et le développement de la partie frontend du site web en utilisant l’ API fournie par l'équipe de développement backend. Cette initiative visait à offrir une expérience utilisateur cohérente entre le site web et l'application mobile, qui fonctionne sous la plateforme Ionic.",
    skill: [
      "React",
      "Typescript",
      "Vite",
      "Redux",
      "Bootstrap",
      "Figma",
      "Api Rest",
      "Sass",
      "Postman",
      "Photoshop",
    ],
    details:
      "Thunder express est un projet de livraison de repas à domicile qui compte plus de 10 000 clients. L'équipe de développement de Thunder Express est composée de deux développeurs front-end, deux développeurs backend, un designer, un product owner et un intégrateur.L'équipe opérationnelle se compose de trois membres responsables de la gestion des commandes, soutenus par une équipe de livreurs.J’ai eu l’occasion d’ observer le fonctionnement de l’équipe chargée de la gestion des commandes et  la coordination des livreurs de l’application mobile ce qui m'a permis de mieux comprendre le processus opérationnel et développer une compréhension du secteur de la livraison.",
    qualite: "AUTONOME ET ESPRIT D'ÉQUIPE",
  },
  {
    compagny: "Webify",
    logo: "",
    periode: "Juin 2022 - Aôut2023",
    poste: "Développeur Fullstack junior",
    description:
      "En tant que développeur Fullstack, j'ai commencé mon parcours sur ce projet en tant que stagiaire, où ma mission principale était l'intégration web. Par la suite, lors de mon embauche, j'ai poursuivi mon travail sur ce projet en prenant en charge la conception de la base de données, la mise en place d'API Platform pour créer une API RESTful, ainsi que le développement frontend et la revue de code.",
    skill: [
      "React",
      "Typescript",
      "Redux",
      "Styled components",
      "Sass",
      "Symfony",
      "Api Platform",
      "Photoshop",
    ],
    details: "",
    qualite: "PERSÉVERANT ET AUTONOME",
  },
  {
    compagny: "Bati Azur Reno",
    logo: "",
    periode: "Juillet 2022 - Septembre 2022",
    poste: "Développeur Wordpress",
    description:
      "J’ai travaillé en collaboration avec le designer et le spécialiste SEO pour créer le site vitrine de l’entreprise j’avais pour mission: Création d'un thème wordpress sur mesure. Utilisation de Elementor pour l’Intégration web et le Responsive design. Optimisation SEO.",
    skill: [
      "Wordpress",
      "Sass",
      "Responsive design",
      "Mobile first",
      "Photoshop",
    ],
    details:
      "Pendant le projet WordPress, j'ai conçu un thème sur mesure en utilisant Hello Elementor comme base. Pour garantir une personnalisation optimale et maintenir les mises à jour futures, j'ai créé un thème enfant qui hérite de la structure du thème de base. Ensuite, j'ai ajouté mon propre style et mes fonctionnalités spécifiques pour répondre aux besoins du projet.",
    qualite: "RIGOUREUX ET PRÉCIS",
  },
  {
    compagny: "Webify",
    logo: "",
    periode: "Septembre 2021 - Novembre 2021",
    poste: "Stagiare",
    description:
      "Pendant ma période de stage chez Webify, j'ai eu l'opportunité précieuse de plonger dans le monde de la conception et du développement web, en me concentrant principalement sur l'intégration web dans un projet React. Cette expérience de stage a été le point de départ de mon parcours et au cours de cette période, j’ai eu l’occasion d'acquérir une compréhension approfondie du cycle de développement web et d'apprendre à collaborer efficacement au sein d'une équipe",
    skill: [
      "React",
      "Bootstrap",
      "Sass",
      "Responsive design",
      "Mobile first",
      "Photoshop",
    ],
    details: "",
    qualite: "Curieux",
  },
  {
    compagny: "Wild Code School",
    logo: "",
    periode: "Novembre 2021 - Février 2022",
    poste: "Développeur Web et Mobile",
    description:
      "Cette formation mettait l'accent sur la pratique et la collaboration. Nous avons travaillé sur plusieurs projets concrets, ce qui m'a permis d'acquérir une expérience pratique et de développer mes compétences en tant que développeur web.",
    skill: [
      "PHP",
      "Symfony",
      "SCRUM",
      "Bootstrap",
      "Sass",
      "Responsive design",
      "Mobile First",
      "Mysql",
    ],
    details: "",
    qualite: "Curieux et Travail d'équipe",
  },
  {
    compagny: "SFR - SCELEC",
    logo: "SFR",
    periode: "Janvier 2019 - Décembre 2020",
    poste: "Coresponsable technique",
    description:
      "Coresponsable technique d’une équipe de 20 techniciens. Gestion des SAV et des réclamations",
    skill: [],
    details: "",
    qualite: "Rigoureux et Esprit d'équipe",
  },
  {
    compagny: "SFR - SCELEC",
    logo: "SFR",
    periode: "Janvier 2014 - Décembre 2020",
    poste: "Téchnicien réseaux",
    description:
      "Installer, vérifier et dépanner les équipements sur site. Remise des systèmes en état de fonctionnement.",
    skill: [],
    details: "Élus meilleur technicien de la région Rhône-Alpes",
    qualite: "Gestion du temps et du stress",
  },
  {
    compagny: "Université de Gênes",
    logo: "genes",
    periode: "Septembre 2009 - Décembre 2012",
    poste: "DUT en Informatique",
    description:
      "Diplôme de 3 ans en ingénierie informatique. Programmation orientée objet administration base de données systèmes d'exploitation et réseaux",
    skill: [],
    details: "",
    qualite: "Autonome et Capacité à apprendre rapidement",
  },
];

export default function Parcours() {
  const [showDetails, setShowDetails] = useState(
    Array(parcoursData.length).fill(false)
  );
  const theme = useContext(ThemeContext);
  const ref = useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["end end", "start start"],
  });
  const cardVariants: Variants = {
    offscreen: {
      y: 300,
      opacity: 0,
    },
    onscreen: {
      y: 0,
      opacity: 1,
      rotate: 0,
      transition: {
        type: "spring",
        bounce: 0.4,
        duration: 0.8,
      },
    },
  };

  return (
    <section className="py-12 lg:py-48">
      <h2 className="text-2xl font-semibold text-center flex flex-col items-center justify-center gap-2 md:text-3xl md:gap-8">
        MON PARCOURS
        <Underline />
      </h2>
      <div className="max-w-[1200px] mx-auto py-[40px] md:mb-[40px] ">
        <div
          ref={ref}
          className="relative flex flex-col gap-24 mx-[30px] md:mr-[80px] md:gap-32 lg:mx-[30px] lg:gap-16"
        >
          {parcoursData.map((d: any, i: number) => (
            <motion.div
              key={i}
              className={`flex flex-col gap-12 left-0 w-[100%] group/item md:relative md:group/item md:w-[100%] md:after:absolute md:after:w-[25px] md:after:h-[3px] md:after:right-[-25px] md:after:top-[25px] ${
                theme === "light"
                  ? "md:after:bg-orange"
                  : "md:after:bg-orangeDark"
              } lg:after:right-[25px] lg:after:even:right-[calc(100%-50px)] lg:w-[50%] lg:even:left-[50%] lg:pr-[50px] lg:even:pl-[50px] lg:even:pr-0 lg:pl-[0px]`}
              initial="offscreen"
              whileInView="onscreen"
              viewport={{ once: true, amount: 1 }}
            >
              <div
                className={`hidden md:absolute md:top-[0px] md:z-30 md:right-[-65px] md:w-[50px] md:h-[50px] md:flex md:items-center md:justify-center md:border-[3px] md:rounded-full ${
                  theme === "light"
                    ? "md:border-orange md:text-dark md:bg-white"
                    : "md:border-orangeDark md:bg-dark"
                } md:font-extrabold md:text-l lg:group lg:right-[-25px] lg:group-even/item:left-[-25px]`}
              >
                AS
              </div>
              <div
                className={`relative overflow-hidden rounded-xl border-[3px] ${
                  theme === "light" ? " border-orange" : "border-orangeDark"
                }`}
              >
                <div
                  className={`p-4 cursor-text text-center m-3 ${
                    theme === "light" ? "bg-gray" : "bg-dark300"
                  }`}
                >
                  <h5 className="p-0 leading-none text-2xl md:text-3xl">
                    {d.compagny}
                  </h5>
                  <span className="text-sm leading-none md:text-base">
                    {d.periode}
                  </span>
                </div>
                <div className="p-4 flex flex-col gap-4 md:p-6 lg:gap-6 lg:py-12">
                  <h5 className="font-light text-xl font-medium cursor-text md:text-2xl">
                    {d.poste}
                  </h5>
                  <ul className="flex gap-8 text-l">
                    <li
                      className={`cursor-pointer ${
                        !showDetails[i] && "border-b-2"
                      } ${
                        theme === "light"
                          ? "border-green200"
                          : "border-orangeDark"
                      } `}
                      onClick={() =>
                        setShowDetails((prevState) =>
                          prevState.map((value, index) =>
                            index === i ? !value : value
                          )
                        )
                      }
                    >
                      Description
                    </li>
                    <li
                      className={`cursor-pointer ${
                        showDetails[i] && "border-b-2"
                      } ${
                        theme === "light"
                          ? "border-green200"
                          : "border-orangeDark"
                      }`}
                      onClick={() =>
                        setShowDetails((prevState) =>
                          prevState.map((value, index) =>
                            index === i ? !value : value
                          )
                        )
                      }
                    >
                      Détails
                    </li>
                  </ul>
                  {!showDetails[i] ? (
                    <>
                      <p className="text-base cursor-text md:text-l ">
                        {d.description}
                      </p>
                      <ul className="flex flex-row flex-wrap gap-2 text-sm cursor-text md:text-base md:gap-4">
                        {d.skill.map((s: any, i: number) => (
                          <li
                            key={i}
                            className={`px-6 py-1 rounded-sm border-[1px] ${
                              theme === "light"
                                ? "border-orange md:hover:bg-orange md:hover:border-orange md:hover:text-white"
                                : "border-orangeDark md:hover:bg-orangeDark md:hover:border-orangeDark"
                            } `}
                          >
                            {s}
                          </li>
                        ))}
                      </ul>
                    </>
                  ) : (
                    <p className="text-l"> {d.details} </p>
                  )}
                </div>
              </div>
              <motion.div
                className="flex flex-row items-center justify-center lg:absolute lg:right-[-100%] lg:group-even/item:left-[-100%] lg:w-full lg:h-full lg:flex lg:items-center lg:justify-center"
                variants={cardVariants}
              >
                <h3 className="text-l flex-1 flex items-center justify-center text-center cursor-text md:text-2xl lg:font-extrabold m-2 p-2 max-w-[250px]">
                  {d.qualite}
                </h3>
              </motion.div>
            </motion.div>
          ))}
          <div
            className={`hidden md:block md:absolute md:w-[3px] md:h-full md:top-[50px] md:mx-[-2px] md:left-[calc(100%+40px)] lg:left-[calc(50%)] ${
              theme === "light" ? "bg-orange" : "bg-orangeDark"
            }`}
          >
            <motion.div
              className={`w-full h-full border-solid border-[1px] origin-bottom ${
                theme === "light"
                  ? "border-orange100 bg-white"
                  : "border-orangeDark bg-dark"
              }`}
              style={{ scaleY: scrollYProgress }}
            ></motion.div>
          </div>
        </div>
      </div>
    </section>
  );
}
