import { Image } from "react-datocms";
import { ProjectI } from "../interfaces/Project.interface";
import { NavLink } from "react-router-dom";
import { Gitlab } from "../assets/Gitlab";
import { Chrome } from "../assets/chrome";

export function ProjectListItem({
  project,
}: {
  project: ProjectI;
  index: number;
}) {
  const { coverImage, title, tech, git, web } = project;
  return (
    <div className="group relative w-[280px] h-[280px] shrink-0 overflow-hidden cursor-pointer rounded-md md:w-[345px] md:h-[345px] lg:w-[250px] lg:h-[250px]">
      <Image data={coverImage.responsiveImage} />
      <NavLink
        to={web}
        target="_blank"
        className={`absolute w-full h-full top-0 lg:hidden`}
      />
      <div className="hidden lg:absolute lg:top-0 lg:w-full lg:h-full lg:flex-col lg:group-hover:flex lg:bg-green200">
        <h5 className="text-dark py-2 font-medium text-center bg-white">
          {title}
        </h5>
        <div className=" flex-1 flex items-center justify-center gap-4">
          {git && (
            <NavLink to={git} target="_blank">
              <div className="w-[36px] h-[36px] flex items-center justify-center border-dark border-2 rounded-full p-[8px] hover:border-white hover:bg-white hover:text-green200">
                <Gitlab />
              </div>
            </NavLink>
          )}
          <NavLink to={web} target="_blank">
            <div className="w-[36px] h-[36px] flex items-center justify-center border-dark border-2 rounded-full p-[8px] hover:border-white hover:bg-white hover:text-green200">
              <Chrome />
            </div>
          </NavLink>
        </div>
        <div className="flex flex-wrap justify-center items-center gap-2 pb-4">
          {tech.map((s, i) => (
            <img
              key={i}
              src={s.url}
              alt={s.alt}
              className="z-30 max-w-[36px]"
            />
          ))}
        </div>
      </div>
    </div>
  );
}
