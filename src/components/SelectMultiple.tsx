import React, { ReactNode } from "react";
import { FieldProps, useField } from "@formiz/core";

export type SelectOption = {
  label?: ReactNode;
  value: string | number;
};

type Value = string[];

export type FieldSelectProps<FormattedValue> = FieldProps<
  Value,
  FormattedValue
> & {
  label?: string;
  helper?: string | ReactNode;
  isRequired?: boolean;
  placeholder?: string;
  size?: "sm" | "md" | "lg";
  autoFocus?: boolean;
  isLoading?: boolean;
  options: SelectOption[];
};

export const FieldSelect = <FormattedValue = Value,>({
  label,
  helper,
  isRequired,
  placeholder,
  size,
  autoFocus = false,
  isLoading = false,
  options,
  ...rest
}: FieldSelectProps<FormattedValue>) => {
  const {
    errorMessage,
    id,
    setValue,
    value,
    shouldDisplayError,
    setIsTouched,
  } = useField(rest);

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValues = Array.from(e.target.selectedOptions, (option) =>
      option.value.toString()
    );
    setValue(selectedValues); // Met à jour la valeur avec un tableau de valeurs sélectionnées
  };
  return (
    <div>
      {label && <label htmlFor={id}>{label}</label>}
      <select
        id={id}
        value={value ?? []} // Utilise un tableau vide si la valeur est null/undefined
        onChange={handleChange} // Utilise la nouvelle fonction de gestion de changement
        onFocus={() => setIsTouched(false)}
        onBlur={() => setIsTouched(true)}
        placeholder={placeholder}
        autoFocus={autoFocus}
        multiple // Active le mode sélection multiple
      >
        {options?.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label ?? option.value}
          </option>
        ))}
      </select>
      {helper && <div>{helper}</div>}
      {shouldDisplayError && <div>{errorMessage}</div>}
    </div>
  );
};
