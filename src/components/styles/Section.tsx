import { ReactNode } from "react";

export default function Section({
  id,
  children,
  bg,
}: {
  id: string;
  children: ReactNode;
  bg: string;
}) {
  return (
    <section
      className={`flex flex-col items-center justify-center py-28 px-12 bg-${bg}`}
      id={id}
    >
      {children}
    </section>
  );
}
