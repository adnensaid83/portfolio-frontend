import { ImageI } from "./Image.interface";

export interface ServiceI {
  id?: string;
  title: string;
  slug: string;
  description: string;
  tag?: { title: string };
  imagesm: ImageI;
  imagelg: ImageI;
  question?: QuestionI[];
}

export interface QuestionI {
  name: string;
  order?: number;
  title: string;
  option: OptionI[];
  choice: string;
}

export interface OptionI {
  label: string;
  value: string;
  order: string;
}
