import { CoverImageI } from "./Project.interface";

export interface TestimonialI {
  fullname: string;
  img: CoverImageI;
  profession: string;
  description: string;
}
