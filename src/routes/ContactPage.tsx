import Layout from "../components/Layout";
import { FieldProps, Formiz, useField, useForm } from "@formiz/core";
import { fibonacci } from "../utils/Fibonacci";

export default function Contact() {
  let sum = 0;
  function calcSum(n: any) {
    for (let i = 0; i < n.length; i++) {
      const digit = parseInt(n[i]);
      if (!isNaN(digit)) {
        sum += digit;
      }
    }
    return sum;
  }
  console.log(calcSum("hi97878"));
  console.log(fibonacci(50));
  const handleSubmit = (values: any) => {
    console.log(values);
  };
  const form = useForm({ onSubmit: handleSubmit });
  return (
    <Layout title="Contact">
      <Formiz connect={form} autoForm>
        <MyField name="feature" label="eden" value="eden" />
        <MyField name="feature" label="jamel" value="jamel" />
        <button type="submit">Submit</button>
      </Formiz>
    </Layout>
  );
}

type MyFieldProps<FormattedValue> = FieldProps<string, FormattedValue> & {
  label: string;
  value: string;
};

export const MyField = <FormattedValue = string,>(
  props: MyFieldProps<FormattedValue>
) => {
  const { id, value, isValid, errorMessage } = useField(props);
  const { label } = props;
  return (
    <>
      <label htmlFor={id}> {label} </label>
      <input
        id={id}
        type="checkbox"
        className="bg-accent"
        value={label}
        onClick={() => {
          return value;
        }}
      />
      {!isValid && <p>{errorMessage}</p>} {/* Display error message */}
    </>
  );
};

/* 

import { Variants } from "framer-motion";
import GeoIcon from "../assets/GeoIcon";
import MailIcon from "../assets/MailIcon";
import PhoneIcon from "../assets/phoneIcon";
import { Underline } from "../components/TitleSection";
import Section from "../components/styles/Section";
import Layout from "../components/Layout";

export default function Contact({
  email,
  address,
  phone,
}: {
  email: string;
  address: string;
  phone: string;
}) {
  const emailVariants: Variants = {
    offscreen: {
      y: 300,
      opacity: 0,
    },
    onscreen: {
      y: 0,
      opacity: 1,
      rotate: 0,
      transition: {
        type: "spring",
        duration: 0.4,
      },
    },
  };
  return (
    <Layout title="Contact">
      <Formiz connect={form}>
        <button type="submit">Submit</button>
      </Formiz>
            <div className="">
        <p className="text-2xl text-center md:text-3xl lg:text-4xl">
          Engagez-moi !
        </p>
        <div className="text-4xl mb-12 text-center md:text-5xl md:mb-16">
          👇
        </div>
        <div className="flex flex-col justify-center gap-12  md:mb-16 md:flex-row lg:gap-44">
          <ContactItem icon={<MailIcon />} label="E-mail" value={email} />
          <ContactItem icon={<PhoneIcon />} label="Téléphone" value={phone} />
          <ContactItem icon={<GeoIcon />} label="Adresse" value={address} />
        </div>
      </div>
    </Layout>
  );
}

type ContactItemProps = {
  icon: any;
  label: string;
  value: string;
};
function ContactItem({ icon, label, value }: ContactItemProps) {
  return (
    <div className="flex gap-4 gap-4">
      <div className="w-[48px] h-[48px] md:w-[60px] md:h-[60px] p-5 rounded-full bg-white drop-shadow-md text-orange border-2 border-white">
        {icon}
      </div>
      <div>
        <h3 className="text-l md:text-xl font-bold"> {label} </h3>
        <address className="not-italic text-l text-center leading-none font-light text-gray100 md:text-xl">
          <a href="mailto:web@adnensaid.fr"> {value} </a>
        </address>
      </div>
    </div>
  );
}

*/
