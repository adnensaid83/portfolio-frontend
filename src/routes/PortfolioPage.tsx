import { useState } from "react";
import Layout from "../components/Layout";
import { RegularList } from "../components/RegularList";
import ArrowLeft from "../components/ArrowLeft";
import ArrowRight from "../components/ArrowRight";
import { Underline } from "../components/TitleSection";
import Loader from "../components/loader";
import { gql, useQuery } from "@apollo/client";
import { ProjectListItem } from "../components/ProjectListItem";
import Markdown from "react-markdown";

export default function PortfolioPage() {
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;
  const { loading, error, data } = useQuery(PROJECTS_QUERY, {
    variables: { first: pageSize, skip: (currentPage - 1) * pageSize },
  });
  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };
  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  if (loading) return <Loader />;
  if (error) return <div>Error</div>;
  console.log(data);
  return (
    <Layout title={data.portfolioPage.bigTitle}>
      <h2 className="flex flex-col gap-6 pb-12 text-3xl lg:text-4xl lg:text-start lg:items-start">
        {data.portfolioPage.smallTitle}
        <Underline />
      </h2>
      <div className="max-w-[800px] mb-12 prose prose-xl prose-p:text-dark prose-p:text-xl prose-p:leading-normal">
        <Markdown>{data.portfolioPage.description}</Markdown>
      </div>
      <ul className="grid grid-cols-1 place-items-center mb-12 gap-x-4 gap-y-8 md:grid-cols-2 lg:grid-cols-4 lg:gap-x-12 lg:gap-y-20">
        <RegularList
          items={data.allProjects}
          resourceName="project"
          itemComponent={ProjectListItem}
        />
      </ul>
      <div className="flex gap-12 justify-center">
        <button
          onClick={handlePrevPage}
          disabled={currentPage === 1}
          className={`w-[48px] h-[48px] p-5 rounded-full bg-white drop-shadow-md border-2 border-white text-orange disabled:text-gray md:w-[60px] md:h-[60px]`}
        >
          <ArrowLeft />
        </button>
        <button
          className={`w-[48px] h-[48px] p-5 rounded-full bg-white drop-shadow-md border-2 border-white text-orange disabled:text-gray md:w-[60px] md:h-[60px]`}
          onClick={handleNextPage}
          disabled={
            currentPage === Math.ceil(data._allProjectsMeta.count / pageSize)
          }
        >
          <ArrowRight />
        </button>
      </div>
    </Layout>
  );
}

const PROJECTS_QUERY = gql`
  query GetPortfolioPage($first: IntType, $skip: IntType) {
    portfolioPage {
      id
      name
      slug
      smallTitle
      bigTitle
      description
    }
    _allProjectsMeta {
      count
    }
    allProjects(first: $first, skip: $skip) {
      id
      title
      description
      thumbnail {
        url
        alt
      }
      coverImage {
        responsiveImage(
          imgixParams: { fit: clip, w: "350", h: "280", auto: format }
        ) {
          srcSet
          webpSrcSet
          sizes
          src
          height
          aspectRatio
          alt
          title
          base64
        }
      }
      maquette {
        url
        alt
      }
      tech {
        url
        alt
      }
      web
      git
    }
  }
`;
