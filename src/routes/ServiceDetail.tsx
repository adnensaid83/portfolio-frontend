import { useParams } from "react-router-dom";
import Layout from "../components/Layout";
import { Underline } from "../components/TitleSection";
import Loader from "../components/loader";
import gql from "graphql-tag";
import { useQuery } from "@apollo/client";
import { Image } from "react-datocms";
import Markdown from "react-markdown";

const ServiceDetail = () => {
  const { slug } = useParams();
  const { loading, error, data } = useQuery(GET_SERVICE_BY_SLUG, {
    variables: { slug },
  });
  if (loading) return <Loader />;
  if (error) return <div>Error:</div>;
  const selectedService = data.allServices[0];
  const { tag, title, description, serviceImage } = selectedService;
  return (
    <Layout title={title} tag={tag?.title}>
      <div className="flex flex-col-reverse items-center gap-12 mb-24 lg:grid lg:grid-cols-2 lg:gap-32">
        <div className="">
          <h2 className="flex flex-col gap-6 items-center text-center pb-12 text-3xl lg:text-4xl lg:text-start lg:items-start">
            {title}
            <Underline />
          </h2>
          <div className="prose prose-xl prose-p:text-dark prose-p:text-xl prose-p:leading-normal text-center font-light mb-0 pb-12 lg:text-start">
            <Markdown>{description}</Markdown>
          </div>
        </div>
        <Image data={serviceImage.responsiveImage} />
      </div>
    </Layout>
  );
};

export default ServiceDetail;

const GET_SERVICE_BY_SLUG = gql`
  query GetServiceBySlug($slug: String!) {
    allServices(filter: { slug: { eq: $slug } }) {
      id
      description
      slug
      title
      tag {
        title
      }
      serviceImage {
        responsiveImage(
          imgixParams: { fit: clip, w: "550", h: "450", auto: format }
        ) {
          srcSet
          webpSrcSet
          sizes
          src
          height
          aspectRatio
          alt
          title
          base64
        }
      }
    }
  }
`;
