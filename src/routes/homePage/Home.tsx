import Loader from "../../components/loader";
import { BannerRecordI } from "./HomeBannerRecord";
import HomeSection from "./HomeSection";
import ScrollSpy from "react-ui-scrollspy";
import { gql, useQuery } from "@apollo/client";
import { HomeHeaderRecord, HomeHeaderRecordI } from "./HomeHeaderRecord";
import { AboutRecordI } from "./HomeAboutRecord";
import { ServiceRecordI } from "./HomeServiceRecord";
import { HomeTestimonialRecordI } from "./HomeTestimonialRecord";
import { ProjectRecordI } from "./HomeProjectRecord";
import { HomeContactRecordI } from "./HomeContactRecord";

export type SectionI =
  | BannerRecordI
  | AboutRecordI
  | ServiceRecordI
  | HomeTestimonialRecordI
  | ProjectRecordI
  | HomeContactRecordI
  | HomeHeaderRecordI;

const Home = () => {
  const { loading, error, data } = useQuery(HOME_QUERY);
  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <div>
      <HomeHeaderRecord
        details={data.homePage.homeDetails.find(
          (d: SectionI) => d.__typename === "HomeHeaderRecord"
        )}
      />
      <ScrollSpy scrollThrottle={30} useBoxMethod={false}>
        {data.homePage.homeDetails.map((section: SectionI) => (
          <HomeSection key={section.id} details={section} />
        ))}
      </ScrollSpy>
    </div>
  );
};

export default Home;

const HOME_QUERY = gql`
  query GetHomePage {
    homePage {
      id
      name
      slug
      homeDetails {
        ... on HomeHeaderRecord {
          id
          __typename
          logo {
            responsiveImage(
              imgixParams: { fit: crop, w: "42", h: "45", auto: format }
            ) {
              srcSet
              webpSrcSet
              sizes
              src
              height
              aspectRatio
              alt
              title
              base64
            }
          }
          sections {
            id
            name
            slug
            order
          }
          social {
            id
            url
            title
          }
        }
        ... on HomeBannerRecord {
          id
          __typename
          bigTitle
          description
          buttonText
          bannerImage {
            responsiveImage(
              imgixParams: { fit: crop, w: "350", h: "350", auto: format }
            ) {
              srcSet
              webpSrcSet
              sizes
              src
              height
              aspectRatio
              alt
              title
              base64
            }
          }
          techLabel
          tech {
            url
            alt
          }
        }
        ... on HomeAboutRecord {
          id
          __typename
          bigTitle
          leftSmallTitle
          leftYear
          leftText
          rightSmallTitle
          rightYear
          rightText
          description
          image {
            responsiveImage(
              imgixParams: { fit: clip, w: "540", h: "360", auto: format }
            ) {
              srcSet
              webpSrcSet
              sizes
              src
              height
              aspectRatio
              alt
              title
              base64
            }
          }
          circletext
        }
        ... on HomeServiceRecord {
          id
          __typename
          bigTitle
          buttonText
          serviceDetails {
            title
            slug
            tag {
              title
            }
            description
            order
          }
        }
        ... on HomeProjectRecord {
          id
          __typename
          bigTitle
          buttontext
          projectDetails {
            title
            description
            thumbnail {
              url
              alt
            }
            coverImage {
              responsiveImage(imgixParams: { fit: clamp, auto: format }) {
                srcSet
                webpSrcSet
                sizes
                src
                width
                height
                aspectRatio
                alt
                title
                base64
              }
            }
            order
            web
            git
            tech {
              url
              alt
            }
          }
        }
        ... on HomeContactRecord {
          id
          __typename
          bigTitle
          smallTitle
          emailLabel
          email
          phoneLabel
          phone
          adresseLabel
          adresse
        }
      }
    }
  }
`;
