import { Container } from "../../components/Container";
import { SplitScreen } from "../../components/SplitScreen";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";
import { CoverImageI } from "../../interfaces/Project.interface";
import ingenieur from "../../assets/ingenieur-logiciel.png";
import { Image } from "react-datocms";
import { CircularText } from "../../components/CircularText";
import { ExperienceIcon } from "../../assets/ExperienceIcon";
import EducationIcon from "../../assets/EducationIcon";
import Markdown from "react-markdown";
export interface AboutRecordI {
  id: string;
  __typename: string;
  bigTitle: string;
  leftSmallTitle: string;
  leftYear: string;
  leftText: string;
  rightSmallTitle: string;
  rightYear: string;
  rightText: string;
  description: string;
  image: CoverImageI;
  circletext: string;
}
export const AboutRecord = ({ details }: { details: AboutRecordI }) => {
  const {
    bigTitle,
    leftSmallTitle,
    leftYear,
    leftText,
    rightSmallTitle,
    rightYear,
    rightText,
    description,
    image,
    circletext,
  } = details;
  return (
    <Section id="a-propos" bg="white">
      <h2 className="flex flex-col items-center justify-center gap-2 text-3xl font-semibold text-center mb-16 md:mb-24 md:text-4.4xl md:gap-8">
        {bigTitle}
        <Underline />
      </h2>
      <Container>
        <SplitScreen
          className="flex flex-col gap-12 lg:flex-row md:gap-24 md:px-24 lg:px-0 lg:mb-[80px]"
          leftStyle="relative flex-1"
          rightStyle="flex-1"
        >
          <AboutLeft image={image} circleText={circletext} icon={ingenieur} />
          <AboutRight
            leftSmallTitle={leftSmallTitle}
            leftYear={leftYear}
            leftText={leftText}
            rightSmallTitle={rightSmallTitle}
            rightYear={rightYear}
            rightText={rightText}
            description={description}
          />
        </SplitScreen>
      </Container>
    </Section>
  );
};

interface AboutLeftProps {
  image: CoverImageI;
  circleText: string;
  icon: any;
}
function AboutLeft({ image, circleText, icon }: AboutLeftProps) {
  return (
    <>
      <div className="overflow-hidden">
        <Image data={image.responsiveImage} />
      </div>
      <div className="hidden lg:block md:absolute md:right-[-50px] md:bottom-[-80px] md:w-[220px] md:h-[220px] md:p-8 md:rounded-full md:shadow-md md:bg-[rgba(255,255,255,.9)] md:border-2 md:border-transparent">
        <div className="spinSlow">
          <CircularText text={circleText} />
        </div>
        <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center">
          <img src={icon} alt="ingénieur" className="w-[50px]" />
        </div>
      </div>
    </>
  );
}

interface AboutRightProps {
  leftSmallTitle: string;
  leftYear: string;
  leftText: string;
  rightSmallTitle: string;
  rightYear: string;
  rightText: string;
  description: string;
}
function AboutRight({
  leftSmallTitle,
  leftYear,
  leftText,
  rightSmallTitle,
  rightYear,
  rightText,
  description,
}: AboutRightProps) {
  return (
    <>
      <div className="flex flex-col mb-12 md:flex-row md:mb-12">
        <div className="flex-1 border-2 border-dark even:border-l-0">
          <div className="group flex flex-col justify-center items-center w-[100%] h-[150px]">
            <div className="max-w-[40px] group-hover:text-orange group-hover:animate-bounce-x">
              <ExperienceIcon />
            </div>
            <h3 className="text-2xl font-medium group-hover:text-orange">
              {leftSmallTitle}
            </h3>
            <p className="font-light text-l mb-0 px-4">{leftYear}</p>
            <p className="font-light text-l mb-0">{leftText}</p>
          </div>
        </div>
        <div className="flex-1 border-2 even:border-t-0 border-dark md:even:border-l-0 md:even:border-t-2">
          <div className="group flex flex-col justify-center items-center w-[100%] h-[150px]">
            <div className="max-w-[40px] text-dark group-hover:text-orange group-hover:animate-bounce-x">
              <EducationIcon />
            </div>

            <h3 className="text-2xl font-medium group-hover:text-orange">
              {rightSmallTitle}
            </h3>
            <p className="font-light text-l mb-0 px-4">{rightYear}</p>
            <p className="font-light text-l mb-0">{rightText}</p>
          </div>
        </div>
      </div>
      <div className="prose prose-xl prose-p:text-dark prose-p:text-base prose-p:leading-normal">
        <Markdown>{description}</Markdown>
      </div>
    </>
  );
}
