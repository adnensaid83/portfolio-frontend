import { Image } from "react-datocms";
import { SplitScreen } from "../../components/SplitScreen";
import { CoverImageI } from "../../interfaces/Project.interface";
import { ImageI } from "../../interfaces/Image.interface";
import { RegularList } from "../../components/RegularList";
import { useState } from "react";
import { NavLink } from "react-router-dom";

// Banner section
export type BannerRecordI = {
  id: string;
  __typename: string;
  bigTitle?: string;
  description?: string;
  buttonText?: string;
  bannerImage: CoverImageI;
  techLabel?: string;
  tech: {
    alt: string;
    url: string;
  }[];
};
export const BannerRecord = ({
  details,
  path,
}: {
  details: BannerRecordI;
  path: string;
}) => {
  const { bigTitle, description, buttonText, bannerImage, techLabel, tech } =
    details;
  return (
    <div id="qui-suis-je" className="py-16 px-12 md:py-28 bg-green">
      <div className="max-w-[993px] mx-auto">
        <SplitScreen
          className="flex items-center flex-col-reverse gap-8 lg:flex-row lg:justify-between lg:gap-32"
          leftStyle="max-w-[500px]"
          rightStyle="relative"
        >
          <BannerLeft
            title={bigTitle ? bigTitle : ""}
            content={description ? description : ""}
            buttonText={buttonText ? buttonText : ""}
            path={path}
          />
          <BannerRight image={bannerImage} />
        </SplitScreen>
        <TechStack title={techLabel} data={tech} />
      </div>
    </div>
  );
};

function BannerRight({ image }: { image: CoverImageI }) {
  return (
    <div className="border-[3px] rounded-full overflow-hidden flex items-center justify-center morphAnimation border-green200 w-[325px] h-[325px] md:w-[350px] md:h-[350px]">
      <Image data={image.responsiveImage} />
    </div>
  );
}

interface BannerLeftProps {
  title: string;
  content: string;
  buttonText: string;
  path: string;
}
function BannerLeft({ title, content, buttonText, path }: BannerLeftProps) {
  return (
    <>
      <h2
        className={`text-4xl font-medium text-center mb-8 md:mb-12 md:leading-none md:text-[60px] pb-0 translate-y-[8px] text-green200 lg:text-start`}
      >
        {title}
      </h2>

      <p className="word-spacing-l text-center mb-0 lg:text-start line-clamp-4">
        {content}
      </p>
      <div className="my-16 md:my-20 text-center lg:text-start">
        <NavLink
          to={path}
          className="bg-orange text-white px-16 py-4 rounded-md text-l border-orange border-2 transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-orange"
        >
          {buttonText}
        </NavLink>
      </div>
    </>
  );
}

function TechStack({ title, data }: { title?: string; data: ImageI[] }) {
  return (
    <div className="pt-6 md:pt-12 lg:flex lg:items-center lg:justify-between">
      <p className="text-center underline underline-offset-4 mb-12 text-dark md:mb-6 lg:text-start lg:m-0 lg:no-underline">
        {title}
      </p>
      <span className="hidden lg:block lg:w-[3px] lg:h-[30px] lg:bg-green200"></span>
      <ul className="relative flex justify-center gap-2 flex-wrap px-12 md:gap-4 lg:pl-0">
        <RegularList
          items={data}
          resourceName="tech"
          itemComponent={TechStackItem}
        />
      </ul>
    </div>
  );
}
function TechStackItem({ tech }: { tech: { alt: string; url: string } }) {
  const [isShowing, setIsShowing] = useState(false);
  const { url, alt } = tech;
  return (
    <li className="">
      <button
        onMouseEnter={() => setIsShowing(true)}
        onMouseLeave={() => setIsShowing(false)}
      >
        <img src={url} alt={alt} />
      </button>
      {isShowing ? (
        <p className="absolute z-10 text-sm font-bold bg-dark text-white py-2 px-8 rounded-xl uppercase">
          {alt}
        </p>
      ) : null}
    </li>
  );
}
