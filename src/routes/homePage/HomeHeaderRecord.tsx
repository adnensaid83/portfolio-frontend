import Navbar from "../../components/Navbar";
import { CoverImageI } from "../../interfaces/Project.interface";
import { SocialI } from "../../interfaces/Social.interface";

// header
export type HomeHeaderRecordI = {
  id: string;
  __typename: string;
  logo: CoverImageI;
  sections: {
    id: string;
    name: string;
    slug: string;
    order: number;
  }[];
  social: SocialI[];
};
export const HomeHeaderRecord = ({
  details,
}: {
  details: HomeHeaderRecordI;
}) => {
  const { logo, sections, social } = details;
  return (
    <Navbar
      theme="light"
      handleTheme={() => {}}
      logo={logo}
      pages={sections ? sections : []}
      address={"Lyon, France"}
      socials={social ? social : []}
      phone={"07 81 15 29 46"}
    />
  );
};
