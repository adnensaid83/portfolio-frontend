import { NavLink } from "react-router-dom";
import { Container } from "../../components/Container";
import { ProjectListItem } from "../../components/ProjectListItem";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";
import { ProjectI } from "../../interfaces/Project.interface";
import { RegularList } from "../../components/RegularList";

export interface ProjectRecordI {
  id: string;
  __typename: string;
  bigTitle: string;
  buttontext: string;
  projectDetails: ProjectI[];
}
export const ProjectRecord = ({
  details,
  bg,
  path,
}: {
  details: ProjectRecordI;
  bg: string;
  path: string;
}) => {
  const { bigTitle, buttontext, projectDetails } = details;
  return (
    <Section id="portfolio" bg={bg}>
      <h2 className="flex flex-col items-center justify-center gap-2 text-3xl font-semibold text-center mb-16 uppercase md:mb-24 md:text-4.4xl md:gap-8">
        {bigTitle}
        <Underline />
      </h2>
      <Container>
        {/* 
        grid-cols-2 place-items-center mb-16 gap-x-4 gap-y-8 md:grid-cols-2 md:mb-24 md:gap-x-0 lg:grid-cols-4 lg:gap-y-12 lg:gap-x-8
        */}
        <div>
          <ul className="grid grid-cols-1 place-items-center gap-6 mb-16 md:grid-cols-2 md:gap-16 md:mb-24 lg:grid-cols-4 lg:gap-16 ">
            <RegularList
              items={projectDetails}
              resourceName="project"
              itemComponent={ProjectListItem}
            />
          </ul>
          <div className="text-center">
            <NavLink
              to={path}
              className="inline-block bg-orange text-white px-16 py-4 rounded-md text-l border-orange border-2 transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-orange"
            >
              {buttontext}
            </NavLink>
          </div>
        </div>
      </Container>
    </Section>
  );
};
