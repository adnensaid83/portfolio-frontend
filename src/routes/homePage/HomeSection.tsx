import { SectionI } from "./Home";
import { BannerRecord, BannerRecordI } from "./HomeBannerRecord";
import { AboutRecord, AboutRecordI } from "./HomeAboutRecord";
import { ServiceRecord, ServiceRecordI } from "./HomeServiceRecord";
import {
  HomeTestimonialRecord,
  HomeTestimonialRecordI,
} from "./HomeTestimonialRecord";
import { ProjectRecord, ProjectRecordI } from "./HomeProjectRecord";
import { HomeContactRecord, HomeContactRecordI } from "./HomeContactRecord";
const isHomeBannerRecord = (details: SectionI): details is BannerRecordI => {
  return details.__typename === "HomeBannerRecord";
};

const isHomeAboutRecord = (details: SectionI): details is AboutRecordI => {
  return details.__typename === "HomeAboutRecord";
};

const isHomeServiceRecord = (details: SectionI): details is ServiceRecordI => {
  return details.__typename === "HomeServiceRecord";
};

const isHomeTestimonialRecord = (
  details: SectionI
): details is HomeTestimonialRecordI => {
  return details.__typename === "HomeTestimonialRecord";
};

const isHomeProjectRecord = (details: SectionI): details is ProjectRecordI => {
  return details.__typename === "HomeProjectRecord";
};

const isHomeContactRecord = (
  details: SectionI
): details is HomeContactRecordI => {
  return details.__typename === "HomeContactRecord";
};

const HomeSection = ({ details }: { details: SectionI }) => {
  if (isHomeBannerRecord(details)) {
    return <BannerRecord details={details} path="/skill" />;
  }
  if (isHomeAboutRecord(details)) {
    return <AboutRecord details={details} />;
  }
  if (isHomeServiceRecord(details)) {
    return <ServiceRecord details={details} />;
  }
  if (isHomeTestimonialRecord(details)) {
    return <HomeTestimonialRecord details={details} />;
  }
  if (isHomeProjectRecord(details)) {
    return <ProjectRecord details={details} bg="white" path="/portfolio" />;
  }
  if (isHomeContactRecord(details)) {
    return <HomeContactRecord details={details} bg="green" />;
  }
  return <></>;
};

export default HomeSection;
