import { NavLink } from "react-router-dom";
import { Container } from "../../components/Container";
import { NavItem } from "../../components/NavItem";
import { RegularList } from "../../components/RegularList";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";
import { ServiceI } from "../../interfaces/Service.interface";

export interface ServiceRecordI {
  id: string;
  __typename: string;
  bigTitle: string;
  buttonText: string;
  serviceDetails: ServiceI[];
}
export const ServiceRecord = ({ details }: { details: ServiceRecordI }) => {
  const { bigTitle, buttonText, serviceDetails } = details;
  return (
    <Section id="services" bg="green">
      <h2 className="flex flex-col items-center justify-center gap-2 text-3xl font-semibold text-center uppercase mb-16 md:mb-24 md:text-4.4xl md:gap-8">
        {bigTitle}
        <Underline />
      </h2>
      <Container>
        <div>
          <ul
            className={`grid grid-cols-1 mb-16 border-b-[3px] border-dark transition ease-out duration-400 md:mb-24 md:grid-cols-1 md:group/item lg:grid-cols-2`}
          >
            <RegularList
              items={serviceDetails}
              resourceName="service"
              itemComponent={NavItem}
            />
          </ul>
          <div className="text-center">
            <NavLink
              to={"/skill"}
              className="inline-block bg-orange text-white px-16 py-4 rounded-md text-l border-orange border-[3px] transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-orange"
            >
              {buttonText}
            </NavLink>
          </div>
        </div>
      </Container>
    </Section>
  );
};
