/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      dark: "#1d1d1d",
      dark100: "#292929",
      dark200: "rgba(32, 32, 32, 1)",
      dark300: "rgba(25, 25, 25, .5)",
      white: "rgba(255, 255, 255, 1)",
      header: "rgba(255, 255, 255, .7)",
      green: "#dcf1e7",
      green100: "#dcf1e7",
      green200: "rgba(40, 170, 159, .8)",
      green300: "rgba(40, 170, 159, 1)",
      orange: "#d4c820",
      orange100: "rgba(213, 199, 32, .8)",
      violet: "#834a9d",
      blue: "#3a34eb",
      gray: "#f5f1f1",
      gray100: "#767676",
      grayModal: "rgba(245, 246, 250, 0.8)",
      accent: "#ff515c",
      sun: "#f5b027",
      moon: "#f4f1c9",
      skeletonLight: "#f4f1c9",
      skeletonDark: "#292929",
      orangeDark: "#55500d",
      greenDark: "rgba(53, 132, 94, 1)",
      green200Dark: "rgba(16, 68, 64, 1)",
      green300Dark: "rgba(16, 68, 64, .8)",
      violetDark: "#341e3f",
      blueDark: "#0d0a68",
      grayDark: "#101421",
    },
    screens: {
      sm: "550px",
      md: "768px",
      lg: "992px",
      xl: "1140px",
      "2xl": "1140px",
    },
    fontSize: {
      sm: "1.28rem",
      base: "1.6rem",
      l: "1.7rem",
      xl: "2rem",
      "2xl": "2.5rem",
      "3xl": [
        "3rem",
        {
          lineHeight: "1.2",
        },
      ],
      "4xl": [
        "4rem",
        {
          lineHeight: "1.2",
        },
      ],
      "4.4xl": [
        "4.4rem",
        {
          lineHeight: "1",
        },
      ],
      "5xl": [
        "5rem",
        {
          lineHeight: "1.2",
          letterSpacing: "-0.01em",
          fontWeight: "500",
        },
      ],
    },
    fontFamily: {
      sans: ["DM Sans", "sans-serif"],
      serif: ["Merriweather", "serif"],
      logo: ["Fuggles", "cursive"],
    },
    extend: {
      boxShadow: {
        "2xl": "24px 24px 0px -8px #d4c820",
        "3xl": "30px 30px 0px -10px #d4c820",
        nav: "0 0 10px rgba(0,0,0,.09)",
      },
      backgroundImage: {
        button: "url('/src/assets/btn.png')",
        "cadre-left": "url('/src/assets/cadreLeft.png')",
        "cadre-top": "url('/src/assets/cadreTop.png')",
        "cadre-middle": "url('/src/assets/cadreMiddle.png')",
        "cadre-left-dark": "url('/src/assets/bgCadre1Dark.png')",
        "cadre-top-dark": "url('/src/assets/bgCadre2Dark.png')",
        "cadre-middle-dark": "url('/src/assets/bgCadre3Dark.png')",
      },
      scale: {
        101: "1.015",
        102: "1.02",
        103: "1.03",
        104: "1.04",
      },
      rotate: {
        10: "5deg",
      },
      animation: {
        "bounce-x": "bouncex 1s ease",
        bounceSlow: "bounceSlow .4s ease-in-out",
      },
      keyframes: {
        bouncex: {
          "0%, 100%": {
            transform: "translateX(0%)",
          },
          "15%": {
            transform: "translateX(-25%) rotate(-5deg)",
          },
          "30%": {
            transform: "translateX(20%) rotate(3deg)",
          },
          "45%": {
            transform: "translateX(-15%) rotate(-3deg)",
          },
          "60%": {
            transform: "translateX(10%) rotate(2deg)",
          },
          "75%": {
            transform: "translateX(-5%) rotate(-1deg)",
          },
        },
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
